# Tutorial: Write an advanced blueprint plugin

This code is an example of a simple Confluence Blueprint. It contains no context provider and therefore no Java classes.

For more information, see: [Tutorial: Write an advanced blueprint plugin][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run
    
 [1]: https://developer.atlassian.com/confdev/confluence-plugin-guide/confluence-blueprints/write-an-advanced-blueprint-plugin
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project