package com.example.plugins.tutorial.confluence.simplebp;

import com.atlassian.confluence.plugins.createcontent.api.events.BlueprintPageCreateEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.spring.scanner.annotation.imports.ConfluenceImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class MyBlueprintListener implements InitializingBean, DisposableBean {

    @ConfluenceImport
    private EventPublisher eventPublisher;
    private static final ModuleCompleteKey MY_BLUEPRINT_KEY = new ModuleCompleteKey("com.example.plugins.tutorial.confluence.simplebp.simplebp", "my-blueprint");
    private static final Logger log = LoggerFactory.getLogger(MyBlueprintListener.class);

    @Inject
    public MyBlueprintListener(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @EventListener
    public void onBlueprintCreateEvent(BlueprintPageCreateEvent event) {
        String moduleCompleteKey = event.getBlueprint().getModuleCompleteKey();

        if (MY_BLUEPRINT_KEY.getCompleteKey().equals(moduleCompleteKey)) {
            //Take some action when
            log.warn("WARN: Created a blueprint.");
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }
}
